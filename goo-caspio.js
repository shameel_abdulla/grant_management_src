<script>
// google maps api call 
alert("goo-caspio.js");
  google.maps.event.addDomListener(window, 'load', initialize);

  let autocomplete;
  let address1Field;
  let address2Field;
  let postalField;

  function initialize() {
    address1Field = document.getElementById("InsertRecordStreetname");
    address2Field = document.querySelector("#InsertRecordUnitAptNumber");
    postalField = document.querySelector("#InsertRecordZip");
    autocomplete = new google.maps.places.Autocomplete(address1Field, {
      componentRestrictions: { country: ["us"] },
      fields: ["address_components", "geometry"],
      types: ["address"],
    });

    autocomplete.addListener("place_changed", fillInAddress);
  }

  function fillInAddress() {
    // Get the place details from the autocomplete object.
    const place = autocomplete.getPlace();
    let address1 = "";
    let postcode = "";

    // Get each component of the address from the place details,
    // and then fill-in the corresponding field on the form.
    // place.address_components are google.maps.GeocoderAddressComponent objects
    // which are documented at [@app:URL_27]
    for (const component of place.address_components) {
      // @ts-ignore remove once typings fixed
      const componentType = component.types[0];

      switch (componentType) {
        case "street_number": {
          address1 = `${component.long_name} ${address1}`;
          break;
        }

        case "route": {
          address1 += component.short_name;
          break;
        }

        case "postal_code": {
          postcode = `${component.long_name}${postcode}`;
          break;
        }

        case "postal_code_suffix": {
          postcode = `${postcode}-${component.long_name}`;
          break;
        }
        case "locality":
          document.querySelector("#InsertRecordCity").value = component.long_name;
          break;
        case "administrative_area_level_1": {
          document.querySelector("#InsertRecordState").value = component.long_name;
          break;
        }
      }
    }

    address1Field.value = address1;
    postalField.value = postcode;
  }
</script>
